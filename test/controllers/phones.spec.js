describe("Phones Controller", function () {
    var//iables
        $rootScope,
        $scope,
        $controller,
        PhonesController,
        Product,
        GetData,
        GetDataServiceSpy
    ;

    const//ants
        ANY_ELEMENT = 'insurance',
        SELECTED_PRODUCT =  {
            contract : {

                12 : {
                    price: 10,
                        months: 12
                },
                24 : {
                    price: 10,
                        months: 24
                }

            },
            internet: {
                0 : {
                    data: 0,
                        price: 0,
                        discount: 3

                },
                250: {
                    data: 250,
                        price: 10,
                        discount: 2
                },
                500: {
                    data: 500,
                        price: 15,
                        discount: 4
                },
                1000: {
                    data: 1000,
                        price: 20,
                        discount: 5
                },
                2000: {
                    data: 2000,
                        price: 25,
                        discount: 6
                },
                5000: {
                    data: 5000,
                        price: 35,
                        discount: 10
                },
                10000: {
                    data: 10000,
                        price: 45,
                        discount: 9
                }
            },
            voice: {
                0 : {
                    minutes: 0,
                        price: 0,
                        discount: 3

                },
                90: {
                    minutes: 90,
                        price: 9,
                        discount: 2
                },
                150: {
                    minutes: 150,
                        price: 12,
                        discount: 2
                },
                200 : {
                    minutes: 200,
                        price: 12,
                        discount: 2
                },
                300: {
                    minutes: 300,
                        price: 18,
                        discount: 3
                },
                noLimit: {
                    minutes: 'Unlimited Calling',
                        price: 24,
                        discount: 0
                }
            },

            phone: {
                name: "Apple Iphone5s 16GB goud",
                    msrp: "699",
                    price: "419.95"
            },

            insurance: {
                none: {
                    name: "no insurance",
                        price: false
                },
                basic: {
                    name: "basic",
                        price: "10.95"
                },
                plus: {
                    name: 'plus',
                        price: "13.95"
                }
            },

            connection: {
                fee : "30"
            }
        },
        ALL_PRODUCTS = [
            {
                contract : {

                    12 : {
                        price: 10,
                        months: 12
                    },
                    24 : {
                        price: 10,
                        months: 24
                    }

                },
                internet: {
                    0 : {
                        data: 0,
                        price: 0,
                        discount: 3

                    },
                    250: {
                        data: 250,
                        price: 10,
                        discount: 2
                    },
                    500: {
                        data: 500,
                        price: 15,
                        discount: 4
                    },
                    1000: {
                        data: 1000,
                        price: 20,
                        discount: 5
                    },
                    2000: {
                        data: 2000,
                        price: 25,
                        discount: 6
                    },
                    5000: {
                        data: 5000,
                        price: 35,
                        discount: 10
                    },
                    10000: {
                        data: 10000,
                        price: 45,
                        discount: 9
                    }
                },
                voice: {
                    0 : {
                        minutes: 0,
                        price: 0,
                        discount: 3

                    },
                    90: {
                        minutes: 90,
                        price: 9,
                        discount: 2
                    },
                    150: {
                        minutes: 150,
                        price: 12,
                        discount: 2
                    },
                    200 : {
                        minutes: 200,
                        price: 12,
                        discount: 2
                    },
                    300: {
                        minutes: 300,
                        price: 18,
                        discount: 3
                    },
                    noLimit: {
                        minutes: 'Unlimited Calling',
                        price: 24,
                        discount: 0
                    }
                },

                phone: {
                    name: "Apple Iphone5s 16GB goud",
                    msrp: "699",
                    price: "419.95"
                },

                insurance: {
                    none: {
                        name: "no insurance",
                        price: false
                    },
                    basic: {
                        name: "basic",
                        price: "10.95"
                    },
                    plus: {
                        name: 'plus',
                        price: "13.95"
                    }
                },

                connection: {
                    fee : "30"
                }
            },

            {
                contract : {

                    12 : {
                        price: 10,
                        months: 12
                    },
                    24 : {
                        price: 10,
                        months: 24
                    }

                },
                internet: {
                    0 : {
                        data: 0,
                        price: 0,
                        discount: 3
                    },
                    250: {
                        data: 250,
                        price: 10,
                        discount: 2
                    },
                    500: {
                        data: 500,
                        price: 15,
                        discount: 4
                    },
                    1000: {
                        data: 1000,
                        price: 20,
                        discount: 5
                    },
                    2000: {
                        data: 2000,
                        price: 25,
                        discount: 6
                    },
                    5000: {
                        data: 5000,
                        price: 35,
                        discount: 10
                    },
                    10000: {
                        data: 10000,
                        price: 45,
                        discount: 9
                    }
                },
                voice: {
                    0 : {
                        minutes: 0,
                        price: 0,
                        discount: 3

                    },
                    90: {
                        minutes: 90,
                        price: 9,
                        discount: 2
                    },
                    150: {
                        minutes: 150,
                        price: 12,
                        discount: 2
                    },
                    200 : {
                        minutes: 200,
                        price: 12,
                        discount: 2
                    },
                    300: {
                        minutes: 300,
                        price: 18,
                        discount: 3
                    },
                    noLimit: {
                        minutes: 'Unlimited Calling',
                        price: 24,
                        discount: 0
                    }
                },

                phone: {
                    name: "Samsung Galaxy",
                    msrp: "423.99",
                    price: "249.95"
                },

                insurance: {
                    none: {
                        name: "no insurance",
                        price: false
                    },
                    basic: {
                        name: "basic",
                        price: "10.95"
                    },
                    plus: {
                        name: 'plus',
                        price: "13.95"
                    }
                },

                connection: {
                    fee : "30"
                }
            },

            {
                contract : {

                    12 : {
                        price: 10,
                        months: 12
                    },
                    24 : {
                        price: 10,
                        months: 24
                    }

                },
                internet: {
                    0 : {
                        data: 0,
                        price: 0,
                        discount: 3

                    },
                    250: {
                        data: 250,
                        price: 10,
                        discount: 2
                    },
                    500: {
                        data: 500,
                        price: 15,
                        discount: 4
                    },
                    1000: {
                        data: 1000,
                        price: 20,
                        discount: 5
                    },
                    2000: {
                        data: 2000,
                        price: 25,
                        discount: 6
                    },
                    5000: {
                        data: 5000,
                        price: 35,
                        discount: 10
                    },
                    10000: {
                        data: 10000,
                        price: 45,
                        discount: 9
                    }
                },
                voice: {
                    0 : {
                        minutes: 0,
                        price: 0,
                        discount: 3

                    },
                    90: {
                        minutes: 90,
                        price: 9,
                        discount: 2
                    },
                    150: {
                        minutes: 150,
                        price: 12,
                        discount: 2
                    },
                    200 : {
                        minutes: 200,
                        price: 12,
                        discount: 2
                    },
                    300: {
                        minutes: 300,
                        price: 18,
                        discount: 3
                    },
                    noLimit: {
                        minutes: 'Unlimited Calling',
                        price: 24,
                        discount: 0
                    }
                },
                phone: {
                    name: "Apple Iphone4s 16GB white",
                    msrp: "350.99",
                    price: "219.95"
                },

                insurance: {
                    none: {
                        name: "no insurance",
                        price: false
                    },
                    basic: {
                        name: "basic",
                        price: "10.95"
                    },
                    plus: {
                        name: 'plus',
                        price: "13.95"
                    }
                },

                connection: {
                    fee : "30"
                }
            }
        ],
        SELECTED_ROW_RESULT = 'success',
        NOT_SELECTED_ROW_RESULT = ''
    ;

    beforeEach( function() {
        module('app');
        inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $controller = $injector.get('$controller');
            Product = $injector.get('Product');
            GetData = $injector.get('GetData');
            GetDataServiceSpy = spyOnAngularService(GetData, 'loadData', { data: ALL_PRODUCTS });
        });
    });

    describe("Phones", function () {
        beforeEach(function() {
            $scope = $rootScope.$new();
            spyOn($rootScope, '$broadcast').andCallThrough();
            PhonesController = $controller('Phones', { $scope: $scope });
        });

        describe("Initialization of products", function () {
            it("Should initialIze the $scope.products", function () {
                $scope.getProducts();
                expect($scope.products).toEqual(ALL_PRODUCTS)
            });

            it("Should add the first Product to the Products service", function () {

               $scope.getProducts();
                expect().toHaveBeenCalledWith(SELECTED_PRODUCT);

            });
        });

        describe("Add Product Function", function () {
            it("Should add the Product to the Products Service", function () {

            });
        });

        describe("Selected Row Function", function () {
            beforeEach(function () {

            });

            it("Should return no class if there isnt any row selected", function () {
                delete Product.product;
                expect($scope.selectedRow(ANY_ELEMENT, SELECTED_PRODUCT.defined.internet)).toBe(NOT_SELECTED_ROW_RESULT);
            });

            it("Should return the correct class depending on the selected row", function () {
                expect($scope.selectedRow(ANY_ELEMENT, SELECTED_PRODUCT.defined.voice)).toBe(SELECTED_ROW_RESULT);
            });

            it("Should return no class if the row is not selected.", function () {
                expect($scope.selectedRow(ANY_ELEMENT, SELECTED_PRODUCT.defined.internet)).toBe(NOT_SELECTED_ROW_RESULT);
            });
        });

    });



});