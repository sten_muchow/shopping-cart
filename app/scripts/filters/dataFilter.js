
app.filter('data', function() {
    return function(input) {
        if(input) {
            return input / 1000 > 1 ? (input / 1000) + "GB" : input + "MB";
        }
    }
});
