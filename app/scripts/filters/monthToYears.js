app.filter('monthToYears', function() {
    return function(input) {
        if(input) {
            return (input/12).toFixed(0);
        }
    }
});

