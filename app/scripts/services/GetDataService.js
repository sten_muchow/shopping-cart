app.factory("GetData", ['$http', '$q', function($http, $q) {
   return {
        loadData: function() {
            var//iable declarations
                defer = $q.defer()
            ;

            $http({
                method: 'GET', url: '/phones'
            })
            .success(function(data) {
                defer.resolve({ data: data });
            })
            .error(function() {
                console.log("error")
            });

            return defer.promise;
        }
    };

}]);