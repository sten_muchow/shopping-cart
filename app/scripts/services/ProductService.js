app.factory('Product', function ($rootScope) {

    var services = {};

    services.addProduct = function(productList) {
        var//iable declarations
            product = productList.data
        ;
        this.product = {
            complete: product,
            defined: {
                phone: product.phone,
                contract: {
                        months:  product.contract[1].months,
                        price: product.contract[1].price
                    },
                data: {
                    data: product.internet[5].data,
                    price: product.internet[5].price,
                    discount: product.internet[5].discount

                },
                voice: {
                    minutes: product.voice[2].minutes,
                    price: product.voice[2].price,
                    discount: product.voice[2].discount
                },
                insurance: false,
                connection: product.connection.fee
            }
        };
        this.updateCart();
    };

    services.updateCart = function() {
        $rootScope.$broadcast('updateProduct')
    };

    services.updateProduct = function(element, product) {
        this.product.defined[element] = product.data;
        this.updateCart();
    };

    services.showProduct = function() {

    };

    return services;

});