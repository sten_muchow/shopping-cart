var myAppDev = angular.module('myAppDev', ['app', 'ngMockE2E']);

myAppDev.run(function($httpBackend) {
    var phones = [
        {
            contract : [
                {
                    price: 10,
                    months: 12
                },
                {
                    price: 10,
                    months: 24
                }
            ],
            data: [
                {
                    data: 0,
                    price: 0,
                    discount: 3

                },
                {
                    data: 250,
                    price: 10,
                    discount: 2
                },
                 {
                    data: 500,
                    price: 15,
                    discount: 4
                },
                 {
                    data: 1000,
                    price: 20,
                    discount: 5
                },
                 {
                    data: 2000,
                    price: 25,
                    discount: 6
                },
                 {
                    data: 5000,
                    price: 35,
                    discount: 10
                },
                {
                    data: 10000,
                    price: 45,
                    discount: 9
                }
            ],
            voice: [
                 {
                    minutes: 0,
                    price: 0,
                    discount: 3

                },
                 {
                    minutes: 90,
                    price: 9,
                    discount: 2
                },
                 {
                    minutes: 150,
                    price: 12,
                    discount: 2
                },
                 {
                    minutes: 200,
                    price: 12,
                    discount: 2
                },
                 {
                    minutes: 300,
                    price: 18,
                    discount: 3
                },
                 {
                    minutes: 'Unlimited Calling',
                    price: 24,
                    discount: 0
                }
            ],

            phone: {
                name: "Apple Iphone5s 16GB goud",
                msrp: "699",
                price: "419.95"
            },

            insurance: [
                {
                    name: "no insurance",
                    price: false
                },
                 {
                    name: "basic",
                    price: "10.95"
                },
                 {
                    name: 'plus',
                    price: "13.95"
                }
            ],

            connection: {
                fee : "30"
            }
        },

        {
            contract : [
                {
                    price: 10,
                    months: 12
                },
                {
                    price: 10,
                    months: 24
                }
            ],
            data: [
                {
                    data: 0,
                    price: 0,
                    discount: 3

                },
                {
                    data: 250,
                    price: 10,
                    discount: 2
                },
                {
                    data: 500,
                    price: 15,
                    discount: 4
                },
                {
                    data: 1000,
                    price: 20,
                    discount: 5
                },
                {
                    data: 2000,
                    price: 25,
                    discount: 6
                },
                {
                    data: 5000,
                    price: 35,
                    discount: 10
                },
                {
                    data: 10000,
                    price: 45,
                    discount: 9
                }
            ],
            voice: [
                {
                    minutes: 0,
                    price: 0,
                    discount: 3

                },
                {
                    minutes: 90,
                    price: 9,
                    discount: 2
                },
                {
                    minutes: 150,
                    price: 12,
                    discount: 2
                },
                {
                    minutes: 200,
                    price: 12,
                    discount: 2
                },
                {
                    minutes: 300,
                    price: 18,
                    discount: 3
                },
                {
                    minutes: 'Unlimited Calling',
                    price: 24,
                    discount: 0
                }
            ],

            phone: {
                name: "Samsung Galaxy",
                msrp: "423.99",
                price: "249.95"
            },

            insurance: [
                {
                    name: "no insurance",
                    price: false
                },
                {
                    name: "basic",
                    price: "10.95"
                },
                {
                    name: 'plus',
                    price: "13.95"
                }
            ],

            connection: {
                fee : "30"
            }
        },

        {
            contract : [
                {
                    price: 10,
                    months: 12
                },
                {
                    price: 10,
                    months: 24
                }
            ],
            data: [
                {
                    data: 0,
                    price: 0,
                    discount: 3

                },
                {
                    data: 250,
                    price: 10,
                    discount: 2
                },
                {
                    data: 500,
                    price: 15,
                    discount: 4
                },
                {
                    data: 1000,
                    price: 20,
                    discount: 5
                },
                {
                    data: 2000,
                    price: 25,
                    discount: 6
                },
                {
                    data: 5000,
                    price: 35,
                    discount: 10
                },
                {
                    data: 10000,
                    price: 45,
                    discount: 9
                }
            ],
            voice: [
                {
                    minutes: 0,
                    price: 0,
                    discount: 3

                },
                {
                    minutes: 90,
                    price: 9,
                    discount: 2
                },
                {
                    minutes: 150,
                    price: 12,
                    discount: 2
                },
                {
                    minutes: 200,
                    price: 12,
                    discount: 2
                },
                {
                    minutes: 300,
                    price: 18,
                    discount: 3
                },
                {
                    minutes: 'Unlimited Calling',
                    price: 24,
                    discount: 0
                }
            ],

            phone: {
                name: "Apple Iphone4s 16GB white",
                msrp: "350.99",
                price: "219.95"
            },

            insurance: [
                {
                    name: "no insurance",
                    price: false
                },
                {
                    name: "basic",
                    price: "10.95"
                },
                {
                    name: 'plus',
                    price: "13.95"
                }
            ],

            connection: {
                fee : "30"
            }
        }
    ];

    // returns the current list of phones
    $httpBackend.whenGET('/phones').respond(phones);

});