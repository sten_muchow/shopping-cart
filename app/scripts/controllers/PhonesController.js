app.controller('Phones', function($rootScope, $scope, GetData, Product) {

    $scope.getProducts = function() {
        GetData.loadData().then(function(products) {
            $scope.products = products.data;
            $scope.addProduct($scope.products[0]);
        });
    };

    $scope.addProduct = function(data) {
        Product.addProduct({ data: data });
    };

    $scope.selectedRow = function(element, product) {
        return (Product.product && product === Product.product.defined[element].name) ? 'success' : '';
    };

    //Initialize Controller
    $scope.getProducts();

});