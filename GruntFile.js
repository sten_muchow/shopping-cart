module.exports = function(grunt) {
    'use strict';

    // Load plugins here
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-open');
    grunt.loadNpmTasks('grunt-ngmin');
    grunt.loadNpmTasks('grunt-docular');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-sass');

    // Configuration goes here
    grunt.initConfig({
        shell: {
            options : {
                stdout: true
            },
            npm_install: {
                command: 'npm install'
            },
            bower_install: {
                command: 'bower install'
            },
            font_awesome_fonts: {
                command: 'cp -R bower_components/font-awesome/font app/font'         //only works on unix(y) systems
            }

        },

        connect: {
            options: {
                base: 'app/'
            },
            webserver: {
                options: {
                    port: 64488,
                    keepalive: true
                }
            },
            devserver: {
                options: {
                    port: 64488
                }
            },
            testserver: {
                options: {
                    port: 9999
                }
            },
            coverage: {
                options: {
                    base: 'coverage/',
                    port: 5555,
                    keepalive: true
                }
            }
        },

        open: {
            devserver: {
                path: 'http://localhost:64488'
            },
            coverage: {
                path: 'http://localhost:5555'
            }
        },


        pkg: require('./package.json'),

        watch: {
            assests: {
                files: ['scripts/**/*.js', 'test/**/*.spec.js', 'GruntFile.js', './css/styles.css'],
                tasks: ['concat']
            }
        },

        jasmine : {
            // Your project's source files
            src : ['bower_components/angular/angular.js', 'bower_components/angular-mocks/angular-mocks.js', 'app/scripts/app.js', 'app/scripts/**/*.js' ],
            // Your Jasmine spec files

            options : {
                specs : 'test/**/*spec.js',
                helpers: 'test/lib/*.js',
                junit: {
                    path: './test/xml'
                }
            }
        },

        concat: {
            script : {
                src: [
                    './bower_components/angular/angular.js',
                    './bower_components/angular-mocks/angular-mocks.js',
                    './app/scripts/app.js',
                    './app/scripts/**/*.js'
                ],
                dest: './app/assets/app.js'

            },
            styles: {
                src: [
                    './app/styles/css/bootstrap.css',
                    './app/styles/css/styles.css'
                ],
                dest: './app/assets/app.css'
            }
        },

        ngmin: {
            angular : {
                src : ['assets/js/app.js'],
                dest : 'assets/js/app.js'
            }

        },

        uglify : {
            options: {
                report: 'min',
                mangle: false
            },
            scripts : {
                files : {
                    './app/assets/app.min.js' : ['./app/assets/app.js']
                }
            }
        },

        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    './app/styles/css/styles.css' : './app/styles/sass/styles.scss',
                    './app/styles/css/bootstrap.css' : './bower_components/bootstrap-sass/lib/bootstrap.scss'
                }
            }

        }
    });


    // Define your tasks here
    grunt.registerTask('install', ['shell:npm_install','shell:bower_install','shell:font_awesome_fonts']);
    grunt.registerTask('dev', ['install', 'concat', 'connect:devserver', 'open:devserver', 'watch:assests']);
    grunt.registerTask('test', ['jasmine']);
    grunt.registerTask('build', ['sass', 'concat', 'ngmin', 'uglify']);
    grunt.registerTask('default', ['dev']);
};